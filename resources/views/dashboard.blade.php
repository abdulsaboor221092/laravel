<!doctype html>
<html>
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <title>ADMIN - DASHBOARD</title>
        <meta name="description" content="CSS Button Switches with Checkboxes and CSS3 Fanciness" />
        <meta name="keywords" content="css3, css-only, buttons, switch, checkbox, toggle, web design, web development" />
        <meta name="author" content="Codrops" />
        <link rel="shortcut icon" href="../favicon.ico"> 
        <link rel="stylesheet" type="text/css" href="css/style.css" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:700,300' rel='stylesheet' type='text/css' />
    
        <style>
            .header{
                border:outset 8px;
                background-color:green;
                color:white;
            }
            .menu{
                padding-top:10px;
                position:relative;
                border:inset 8px;
                height:100%;
                background-image: linear-gradient(to bottom ,white, grey);
            }
            .content{
                position:relative;
                border:inset 8px;
                height:100%;
                width:100%;
                padding-top:10px;
                background-image: linear-gradient(to bottom, green,white,white,green);
                display:flex;
            }
            .item{
                border:outset 6px ;
            }
            .body{
                display:flex;
                height:100%;
            }
            .logout{
                position:absolute;
                text-align:right;
                width:100%;
                padding-right:20px;
            }
            .body_heading{
                position:absolute;
                text-align:center;
                width:100%;
                padding-right:20px;
            }
            button{
                color:black;
                height:24px;
                padding:2px;
            }
            button:hover{
                background-color:green;
                color:white;
                height:24px;
                padding:2px;
            }
            html, 
            body {
                height: 100%;
            }
        </style>
    </head>
    <body>
        
        <div class="header">
            <header>
                <h1><center>WELCOME "{{Auth::user()->name}}" (PLANTING Dashboard)</center></h1>
            </header>
        </div>
        <div class="body">
            <div class="menu">
                <div class="heading">
                    <h1><center>MENU</center></h1>
                </div>
                <div class="item">
                    <a href="/admin"><h3 style="padding:4px;"><center>PAGE1:"{{$value}}" </center></h3></a>
                </div>
                <br>
                <div class="item">
                    <a href="/"><h3 style="padding:4px;"><center>PAGE2:"{{$value1}}" </center></h3></a>
                </div>
            </div>
            <div class="content">
                <div class="body_heading">
                        <h2><center>MY PLANTS</center></h2>
                </div>
                <div class="logout">
                        <a href="/logout"><button>LOGOUT</button></a>
                </div>
            </div>
        </div>
    </body>
</html>